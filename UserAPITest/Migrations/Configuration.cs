namespace UserAPITest.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<UserAPITest.Data.UserAPITestContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(UserAPITest.Data.UserAPITestContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Users.AddOrUpdate(x => x.Id,
                new Models.User()
                {
                    Id = 1,
                    DateOfBirth = new DateTime(1989,04, 22),
                    Email = "timsmith@gmail.com",
                    FirstName = "tim",
                    LastName = "smith",
                    Modified = new DateTime(1989, 04, 22),
                    MobileNumber = "05551"
                });
        }
    }
}
